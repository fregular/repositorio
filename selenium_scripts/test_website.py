import unittest
from selenium import webdriver
from time import sleep
import os

get_username = os.getenv('username')
get_password = os.getenv('password')

class RepositorioTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver')
        driver = self.driver
        driver.implicitly_wait(30)
        driver.maximize_window()
        driver.get('http://localhost:8000')

    def test_login(self):
        driver = self.driver
        login = driver.find_element_by_xpath('/html/body/nav/div/ul/li[1]/a')
        self.assertTrue(login.is_displayed() and login.is_enabled())
        login.click()
        sleep(1)

        self.assertEqual('Ingrese', driver.title )
        username = driver.find_element_by_id('id_username')
        password = driver.find_element_by_id('id_password')
        submit = driver.find_element_by_xpath('/html/body/form/button')

        self.assertTrue(username.is_enabled() and password.is_enabled() and submit.is_enabled())
        username.send_keys(get_username)
        password.send_keys(get_password)
        submit.click()
        sleep(1)
        documents = driver.find_element_by_xpath('/html/body/nav/div/ul/li[3]/a')
        self.assertTrue(documents.is_enabled())
        documents.click()
        sleep(1)
        create_doc = driver.find_element_by_xpath('/html/body/div/div[2]/a')
        create_doc.click()
        sleep(1)
        name_doc = driver.find_element_by_id('id_name').send_keys("test_selenium")
        content = driver.find_element_by_id('id_content').send_keys("content selenium")
        upload = driver.find_element_by_id('id_display_picture').send_keys(os.getcwd()+"/selenium_scripts/test_upload.txt")
        sleep(1)
        submit_button = driver.find_element_by_name('register').click()
        sleep(1)
        driver.find_element_by_xpath('/html/body/center/div/a').click()
        sleep(3)

    def tearDown(self):
        self.driver.close()
    
if __name__ == "__main__":
    unittest.main(verbosity=2)