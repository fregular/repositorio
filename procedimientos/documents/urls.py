from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from .views import DocDelete

from .views import SearchResultsView

urlpatterns = ([
    path('', views.list_document, name ="list_document"),
    path('create/', views.create_profile, name ="create"),
    path('details/', views.list_document, name ="documents"),
    path('search/', SearchResultsView.as_view(), name ="search"),
    path('delete/<int:pk>', DocDelete.as_view(), name='delete'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT), 'doc')