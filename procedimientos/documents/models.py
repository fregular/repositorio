from django.db import models
from django.utils.timezone import now

# Create your models here.
class User_Profile(models.Model):
    name = models.CharField(max_length=200, verbose_name="Nombre")
    content = models.CharField(max_length=200, verbose_name="Contenido")
    display_picture = models.FileField(verbose_name="Suba el documento")
    created = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now = True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "Documento"
        verbose_name_plural = "Documentos"
        ordering = ["-created"]
        
    def __str__(self):
        return self.name