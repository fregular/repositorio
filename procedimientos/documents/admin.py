from django.contrib import admin
from .models import User_Profile
# Register your models here.

class DocumentAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')
    list_display = ('name','updated')


admin.site.register(User_Profile, DocumentAdmin)
