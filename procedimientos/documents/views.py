from django.shortcuts import render
# Create your views here.
from .forms import Profile_Form
from .models import User_Profile
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView
from .models import User_Profile
from django.db.models import Q # new
from django.views.generic.edit import DeleteView


def list_document(request):
    return render(request, 'documents/list_documents.html')

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']

def create_profile(request):
    form = Profile_Form()
    if request.method == 'POST':
        form = Profile_Form(request.POST, request.FILES)
        if form.is_valid():
            user_pr = form.save(commit=False)
            user_pr.display_picture = request.FILES['display_picture']
            file_type = user_pr.display_picture.url.split('.')[-1]
            file_type = file_type.lower()
            #if file_type not in IMAGE_FILE_TYPES:
            #    return render(request, 'documents/error.html')
            user_pr.save()
            return render(request, 'documents/details.html', {'user_pr': user_pr})
    context = {"form": form,}
    return render(request, 'documents/create.html', context)

def list_document(request):
    documents = User_Profile.objects.all()
    return render(request, 'documents/list_documents.html', {'documents':documents})

class SearchResultsView(ListView):
    model = User_Profile
    template_name = 'documents/search.html'

    def get_queryset(self): # new
        query = self.request.GET.get('q')
        object_list = User_Profile.objects.filter(
            Q(name__icontains=query)
        )
        return object_list    

class DocDelete(DeleteView):
    model = User_Profile
    success_url = reverse_lazy('doc:list_document')