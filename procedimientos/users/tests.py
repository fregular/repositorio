from django.test import TestCase

# Create your tests here.

import unittest
from django.test import Client

from django.core.files import File
import mock
from documents.models import User_Profile

class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_details(self):
        # Issue a GET request.
        response = self.client.get('/users/')
        response1 = self.client.get('/documentos/')
        response2 = self.client.get('/')


        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)

        

    def test_file_field(self):
        file_mock = mock.MagicMock(spec=File)
        file_mock.name = 'test.pdf'
        file_model = User_Profile(display_picture=file_mock)
        self.assertEqual(file_model.display_picture.name, file_mock.name)


class DocumentTestCase(TestCase):
    def setUp(self):
        User_Profile.objects.create(name="test",content="content test")

    def test_create_doc(self):
        user = User_Profile.objects.get(name="test")
        self.assertEqual(str(user.name), "test" )
        self.assertEqual(str(user.content), "content test")


    